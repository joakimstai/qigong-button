const noble = require('noble')
const Player = require('player')

const SERVICE_UUID_UART = '6e400001b5a3f393e0a9e50e24dcca9e'
const CHARACTERISTIC_UUID_TX = '6e400002b5a3f393e0a9e50e24dcca9e'
const CHARACTERISTIC_UUID_RX = '6e400003b5a3f393e0a9e50e24dcca9e'

// The UUID of the qigong button
const PERIPHERAL_UUID_QIGONG = 'e0a62d78bc174150abe352561089738a'

let playing = false
let qigong = new Player('./qigong.mp3')
qigong.on('playing', () => playing = true)
qigong.on('finish', () => playing = false)
qigong.on('error', () => null) // Buggy module, this is required…

/**
 * Play/pause the qigong track.
 */
function playpause() {
  qigong[playing ? 'pause' : 'play']()
}

/**
 * Start scanning for Qigong button.
 */
function scan() {
  // console.log('Scanning…')
  noble.startScanning([SERVICE_UUID_UART])
}

noble.on('stateChange', (state) => {
  if (state === 'poweredOn') {
    scan()
  }
  else {
    noble.stopScanning()
  }
})

noble.on('discover', (peripheral) => {
  // Only connect to the Qigong button
  if (peripheral.uuid !== PERIPHERAL_UUID_QIGONG) return

  // console.log(`Found ${peripheral.advertisement.localName}`)
  
  noble.stopScanning()

  if (peripheral.advertisement.txPowerLevel < 20) {
    // console.log('Low battery')
  }

  peripheral.once('disconnect', (err) => {
    if (err) console.error(err)
    // console.log('Disconnected')

    // Start scanning again to automatically reconnect when connection is lost,
    // but wait a second for the button to disconnect completely
    setTimeout(scan, 1000)
  })

  peripheral.connect((err) => {
    if (err) return console.error(err)
    // console.log('Connected')

    console.log('Button pressed')
    playpause()

    peripheral.disconnect()
  })
})
