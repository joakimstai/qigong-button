import commonjs from 'rollup-plugin-commonjs'
import espruino from 'rollup-plugin-espruino'
// import uglify from 'rollup-plugin-uglify'
// import { minify } from 'uglify-js'

export default {
  entry: 'src/main.js',
  dest: 'dist/bundle.js',
  format: 'es',
  plugins: [
    commonjs(),
    // uglify({
    //   mangle: {
    //     toplevel: true,
    //     except: ['onInit'],
    //   },
    // }, minify),
    espruino({
      // output: true,
      // save: true,
    }),
  ],
}
