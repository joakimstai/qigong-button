let connectionTimeout;

function onInit() {
  setWatch((e) => {
    let longPress = (e.time - e.lastTime) > 1;

    if (longPress) {
      NRF.setLowPowerConnection(false);
      NRF.wake();
      reset();
    }
    else {
      NRF.wake();
      LED3.write(1);

      connectionTimeout = setTimeout(() => {
        NRF.sleep();
        LED3.write(0);
        LED1.write(1);
        setTimeout(() => LED1.write(0), 1000);
      }, 5000);
    }
  }, BTN, {
    edge: 'falling',
    debounce: 20,
    repeat: true
  });

  NRF.sleep();

  NRF.setLowPowerConnection(true);

  NRF.setAdvertising({
    0x180F: [Puck.getBatteryPercentage()]
  }, {
    name: 'Qigong button',
    showName: true,
    discoverable: true,
    interval: 100
  });

  NRF.on('connect', () => {
    if (connectionTimeout) clearTimeout(connectionTimeout);
  });

  NRF.on('disconnect', () => {
    LED3.write(0);
    NRF.sleep();
  });
}

onInit();
