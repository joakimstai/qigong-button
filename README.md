# Qigong Button for [Puck.js](http://puck-js.com)

> Now, let's begin to practice qigong…

See [index.html](index.html) for Web Bluetooth version, [index.js](index.js) for
Node version.

Try the Web Bluetooth version here:  
[qigong-button.netlify.com](https://qigong-button.netlify.com/)
