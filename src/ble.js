let enabled = false

export function isEnabled() {
  return enabled
}

export function enable() {
  NRF.wake()
  enabled = true
}

export function disable() {
  NRF.sleep()
  enabled = false
}

export function init() {
  disable()

  NRF.setLowPowerConnection(true)

  NRF.setAdvertising({
    0x180F: [Puck.getBatteryPercentage()]
  }, {
    name: 'Qigong button',
    showName: true,
    discoverable: true,
    interval: 100
  })
}
