const f = require('Flash')

export default function eraseFlash() {
  f.erasePage((120 - 3) * 4096)
  f.erasePage((120 - 2) * 4096)
  f.erasePage((120 - 1) * 4096)
}
