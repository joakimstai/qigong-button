let leds = [LED, LED1, LED2, LED3]
let interval

function on(ms) {
  if (interval) clearInterval(interval)
  this.write(1)
  if (ms) setTimeout(() => this.write(0), ms)
}

function off(ms) {
  if (interval) clearInterval(interval)
  this.write(0)
  if (ms) setTimeout(() => this.write(1), ms)
}

function blink(ms) {
  let state
  interval = setInterval(() => this.write(state = !state), ms || 200)
}

export default function led(key) {
  if (key === undefined) key = 0
  let pin = leds[key]

  return {
    on: on.bind(pin),
    off: off.bind(pin),
    blink: blink.bind(pin),
  }
}

export function define(def) {
  leds = def
}
